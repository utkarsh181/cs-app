/* count_clock.c --- Get clock cycles to compute Prefix Sum */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define MAX_VAL 10		/* Maximum value for float array */

/* Return CPU cycles since last reset. */
uint64_t
rdtsc()
{
  uint64_t lo, hi;
  __asm__ volatile("rdtsc" : "=a" (lo), "=d" (hi));
  return (lo | (hi << 32));
}

/* Initialize ARR of size N. */
void
init_arr (float arr[], long n)
{
  long i;
  
  srand(1);
  for (i = 0; i < n; i++)
    arr[i] = (float) (rand () % MAX_VAL);
}

/* Compute prefix sum of vector a */
void psum(float a[], float p[], long n)
{
  long i;
  p[0] = a[0];
  for (i = 1; i < n; i++)
    p[i] = p[i-1] + a[i];
}

int
main (int argc, char *argv[])
{
  if (argc != 2)
    {
      fprintf (stderr, "error: invalid number of arguments.\n");
      fprintf (stderr,
	       "Usage: %s N, where N is the number of elements for Prefix Sum.\n",
	       argv[0]);
      return 1;
    }

  uint64_t x, y;
  long n;
  float *a, *p;

  n = atol(argv[1]);
  a = (float *) calloc (n, sizeof(float));
  p = (float *) calloc (n, sizeof(float));
  init_arr (a, n);
  init_arr (p, n);
  x = rdtsc();
  psum (a, p, n);
  y = rdtsc();
  printf ("%ld\n", y-x);
  return 0;
}
