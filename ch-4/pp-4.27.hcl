word Stat = [
     icode == IHALT: SHLT;
     imem_error || dmem_error: SADR;
     !instr_valid: SINS;
     1: SAOK;
]