rproduct:
	andq	%rsi, %rsi
	jle	.L3		; FIXME 2022-04-11: How can we compare with 1?
	pushq	%rbx
	mrmovq	(%rdi), %rbx
	irmovq  $8, %r8
	irmovq  $1, %r9
	subq	%r9, %rsi
	addq	%r8, %rdi
	call	rproduct
	imulq	%rbx, %rax
	popq	%rbx
	ret
.L3:
	irmovl	$1, %rax
	ret
