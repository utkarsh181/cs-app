#include <stdio.h>

#define M 0x1f
#define N 0x8

int
arith (int x, int y)
{
  return x * M + y / N;
}

int
optarith (int x, int y)
{
  int t;

  t = x;
  x <<= 5;
  x -= t;

  if (y < 0)
    y += 7;
  y >>= 3;
  
  return x + y;
}

int
main (void)
{
  int x, y;

  x = 2;
  y = 1;
  printf ("%d\n", arith (x, y));
  printf ("%d\n", optarith (x, y));
  return 0;
}
