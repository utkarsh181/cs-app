#include <stdio.h>

unsigned
bits_set (unsigned x, unsigned m)
{
  return x | m;
}

unsigned
bits_clear (unsigned x, unsigned m)
{
  return x & ~m;
}

unsigned
bits_or (unsigned x, unsigned y)
{
  return bits_set (x, y);
}

unsigned
bits_xor (unsigned x, unsigned y)
{
  return bits_set (bits_clear (x, y), bits_clear (y, x));
}

int
main (void)
{
  unsigned x, y;

  x = 0x23;
  y = 0x55;
  printf ("x | y = %u\nbits_or (x, y) = %u\n\n", x | y, bits_or (x, y));
  printf ("x ^ y = %u\nbits_xor (x, y) = %u\n", x ^ y, bits_xor (x, y));
  return 0;
}
