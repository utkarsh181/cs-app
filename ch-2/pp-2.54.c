#include <stdio.h>
#include <limits.h>
#include <float.h>

int
main (void)
{
  int x;
  float f;
  double d;

  x = INT_MAX;
  printf ("%d\n", x == (int) (float) x);

  d = DBL_MAX;
  printf ("%d\n", d == (double) (float) d);

  f = FLT_MIN;
  printf ("%d\n", f == -(-f));

  printf ("%d\n", 1.0/2 == 1/2.0);
  return 0;
}
