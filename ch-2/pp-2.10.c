#include <stdio.h>

void
inplace_swap (int *x, int *y)
{
  *y = *x ^ *y;
  *x = *x ^ *y;
  *y = *x ^ *y;
}

void
reverse (int arr[], int len)
{
  int i, j;

  for (i = 0, j = len - 1; i < j; i++, j--)
    inplace_swap (&arr[i], &arr[j]);
}

int
main (void)
{
  int arr[] = {1, 2, 3, 4, 5};
  int i, len;

  len = sizeof (arr) / sizeof (int);
  reverse (arr, len);
  for (i = 0; i < len; i++)
    printf (i != 0 ? " %d" : "%d", arr[i]);
  printf ("\n");
  
  return 0;
}
