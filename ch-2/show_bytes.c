#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "show_bytes.h"

/* Show starting LEN bytes of START. */
void
show_bytes (byte_pointer start, int len)
{
  int i;

  for (i = 0; i < len; i++)
    {
      if (i == 0)
	printf ("%.2x", start[i]);
      else
	printf (" %.2x", start[i]);
    }
  printf ("\n");
}

/* Show bytes of integer X. */
void
show_int (int x)
{
  show_bytes ((byte_pointer) &x, sizeof (int));
}

/* Show bytes of floating point number X. */
void
show_float (float x)
{
  show_bytes ((byte_pointer) &x, sizeof (float));
}

/* Show bytes of floating point number X. */
void
show_double (double x)
{
  show_bytes ((byte_pointer) &x, sizeof (double));
}

/* Show bytes of pointer X. */
void
show_pointer (void *x)
{
  show_bytes ((byte_pointer) &x, sizeof (void *));
}

void
show_string (char *s)
{
  show_bytes ((byte_pointer) s, strlen (s));
}
