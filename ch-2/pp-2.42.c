#include <stdio.h>

int
div16 (int x)
{
  int k;

  k = 4;
  return (x < 0 ? x + (1 << k) - 1 : x) >> k;
}

int
main (void)
{
  int x;

  x = -12340;
  printf ("%d\n", div16 (x));
  return 0;
}
