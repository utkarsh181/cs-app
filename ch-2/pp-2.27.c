#include <stdio.h>
#include <limits.h>

/* Is adding unsigned numbers X and Y valid?  That is, on adding these numbers
   result should not overflow. */
int
is_uadd_valid (unsigned x, unsigned y)
{
  return x + y >= x;
}

int
main (void)
{
  unsigned x, y;

  x = 1;
  y = UINT_MAX;
  printf ("%d\n", is_uadd_valid (x, y));
  return 0;
}
