#include <stdio.h>
#include <limits.h>

enum errors { NEG_OVERFLOW = 1, POS_OVERFLOW, ADD_INV };

/* Is adding numbers X and Y valid?  That is, on adding these numbers result
   should not positive and negative overflow. */
int
is_add_valid (int x, int y)
{
  int z = x + y;

  if (x < 0 && y < 0 && z >= 0)	/* Negative Overflow. */
    return NEG_OVERFLOW;
  else if (x > 0 && y > 0 && z < 0) /* Positive Overflow. */
    return POS_OVERFLOW;
  else
    return 0;
}

/* Is subtracting number X and Y valid? */
int
is_sub_valid (int x, int y)
{
  if (y == INT_MIN)		/* INT_MIN is an additive inverse of itself. */
    return ADD_INV;
  else
    return is_add_valid (x, -y);
}

int
main (void)
{
  int x, y;

  x = INT_MIN;
  y = INT_MIN;
  printf ("%d\n", is_add_valid (x, y));
  return 0;
}
