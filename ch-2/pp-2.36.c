#include <stdio.h>
#include <limits.h>

/* Is multiplying numbers X and Y valid?  That is, on multiplying these numbers
   result should not overflow. */
int
is_mul_valid (int x, int y)
{
  long long z;
  
  z = (long long) x * y;  /* Cast will ensure lossless initialization. */
  return z >= INT_MIN && z <= INT_MAX;
}

int
main (void)
{
  int x, y;

  x = INT_MIN;
  y = INT_MIN;
  printf ("%d\n", is_mul_valid (x, y));
  return 0;
}
