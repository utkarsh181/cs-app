#include <stdio.h>

float
array_sum (float arr[], size_t len)
{
  size_t i;
  float sum;

  for (i = 0, sum = 0.0; i < len; i++)
    sum += arr[i];
  return sum;
}

int
main (void)
{
  float arr[] = {1.0, 2.0, 3.0, 11.0};
  printf ("%g\n", array_sum (arr, sizeof (arr) / sizeof (float)));
  return 0;
}
