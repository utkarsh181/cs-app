#ifndef SHOW_BYTES_H
#define SHOW_BYTES_H

typedef unsigned char *byte_pointer;

void show_int (int);
void show_float (float);
void show_double (double);
void show_pointer (void *x);
void show_string (char *);

#endif
