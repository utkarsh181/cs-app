#define _GNU_SOURCE 1

#include <stdio.h>
#include <math.h>
#include "show_bytes.h"

#define POS_INFINITY	1.8e308 + 1
#define NEG_INFINITY	(-POS_INFINITY)
#define NEG_ZERO	(-0.0)

int
main (void)
{
  show_double (INFINITY);
  show_double (POS_INFINITY);
  show_double (NEG_INFINITY);
  show_double (NEG_ZERO);
  return 0;
}
