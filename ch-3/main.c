#include <stdio.h>
#include "mstore.h"

long
mult2(long a, long b)
{
  return a * b;
}

int
main(void)
{
  long d;

  multstore(2, 3, &d);
  printf("2 * 3 --> %ld\n", d);
  return 0;
}
