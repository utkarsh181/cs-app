typedef char src_t;
typedef short dest_t;

void
solve (src_t *srcp, dest_t *destp)
{
  *destp = (dest_t) *srcp;
}

