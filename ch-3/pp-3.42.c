struct ACE
{
  short v;
  struct ACE *p;
};

short
test (struct ACE *ptr)
{
  short val = 1;
  while (ptr)
    {
      val *= ptr->v;
      ptr = ptr->p;
    }
  return val;
}
