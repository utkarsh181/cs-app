/* pp-3.14.c -- Practise problem to learn TEST instruction. */

/* Data types corresponding to every sub-problem. */
typedef long data_a_t;
typedef short data_b_t;
typedef unsigned char data_c_t;
typedef int data_d_t;

/* Test operator corresponding to every sub-problem. */
#define TEST_A >=
#define TEST_B ==
#define TEST_C >
#define TEST_D <=

int
test_a (data_a_t a)
{
  return a TEST_A 0;
}

int
test_b (data_b_t a)
{
  return a TEST_B 0;
}

int
test_c (data_c_t a)
{
  return a TEST_C 0;
}

int
test_d (data_d_t a)
{
  return a TEST_D 0;
}
