long
solve(long x, char n)
{
  x <<= 4;
  x >>= n;
  return x;
}
