void
decode1 (long *xp, long *yp, long *zp)
{
  long t1, t2, t3;

  t1 = *xp;
  t2 = *yp;
  t3 = *zp;
  *yp = t1;
  *zp = t2;
  *xp = t3;
}
