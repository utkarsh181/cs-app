#include <stdio.h>

long
test_one (unsigned long x)
{
  long val = 1;
  while (x)
    {
      val ^= x;
      x >>= 1;
    }
  return val & 0;
}

int
main (void)
{
  printf ("%ld\n%ld\n", test_one (2), test_one (3));
  return 0;
}
