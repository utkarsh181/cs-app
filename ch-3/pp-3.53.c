typedef int arg1_t;
typedef long arg2_t;
typedef float arg3_t;
typedef double arg4_t;

double
funct1 (arg1_t p, arg2_t q, arg3_t r, arg4_t s)
{
  return p / (q + r) - s;
}
