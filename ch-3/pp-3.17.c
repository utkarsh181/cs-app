int lt_cnt = 0;			/* Less-than count. */
int ge_cnt = 0;			/* Greater-than-equal count. */

long
diff_se_goto (long x, long y)
{
  long result;

  if (x < y)
    goto x_le_y;
  ge_cnt++;
  result = x - y;
  return result;
x_le_y:
  lt_cnt++;
  result = y - x;
  return result;
}
