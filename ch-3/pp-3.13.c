/* pp-3.13.c -- Practise problem to learn CMP instruction. */

/* Data types corresponding to every sub-problem. */
typedef int data_a_t;
typedef short data_b_t;
typedef unsigned char data_c_t;
typedef long data_d_t;

/* Comparison operator corresponding to every sub-problem. */
#define COMP_A <
#define COMP_B >=
#define COMP_C <=
#define COMP_D !=

int
comp_a (data_a_t a, data_a_t b)
{
  return a COMP_A b;
}

int
comp_b (data_b_t a, data_b_t b)
{
  return a COMP_B b;
}

int
comp_c (data_c_t a, data_c_t b)
{
  return a COMP_C b;
}

int
comp_d (data_d_t a, data_d_t b)
{
  return a COMP_D b;
}
