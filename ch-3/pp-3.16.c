/* pp-3.16.c -- Implementing conditional with assembly-styled GOTO code. */

void
cond (short a, short *p)
{
  if (a && *p < a)
    *p = a;
}

void
cond_goto (short a, short *p)
{
  if (!a)
    goto exit;
  if (*p >= a)
    goto exit;
  *p = a;
 exit:
  return;
}
