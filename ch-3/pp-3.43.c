typedef union
{
  struct
  {
    long u;
    short v;
    char w;
  } t1;
  struct
  {
    int a[2];
    char *p;
  } t2;
} u_type;

void
get_long (u_type * up, long *dest)
{
  *dest = up->t1.u;
}

void
get_short (u_type * up, short *dest)
{
  *dest = up->t1.v;
}

void
get_char_ptr (u_type * up, char *dest)
{
  dest = &up->t1.w;
}

void
get_int_ptr (u_type * up, int *dest)
{
  dest = up->t2.a;
}

void
get_int (u_type * up, int *dest)
{
  *dest = up->t2.a[up->t1.u];
}

void
get_char (u_type * up, char *dest)
{
  *dest = *up->t2.p;
}
