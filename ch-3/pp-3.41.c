struct test
{
  short *p;
  struct
  {
    short x;
    short y;
  } s;
  struct test *next;
};

void
st_init (struct test *st)
{
  st->s.y = st->s.x;
  st->p = &(st->s.y);

  /* FIXME 2022-03-09: This is generating: movq %rdi, 16(%rdi).  Is memory
     alignment a factor here?*/
  st->next = st;
}
