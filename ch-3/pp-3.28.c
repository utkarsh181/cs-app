long
test_two (unsigned long x)
{
  long val = 0, i;
  
  for (i = 64; i != 0; i--)
    {
      val = (2*val) | (x & 1);
      x >>= 1;
    }
  return val;
}
