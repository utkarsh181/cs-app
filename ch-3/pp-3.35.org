#+title: Practice Problem 3.35
#+author: Utkarsh Singh

Calle-saved register =%rbx= is used to store value of =x=, which is
used by expression: =return x + rv=.
