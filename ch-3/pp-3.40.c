#define N 16

typedef int fix_matrix[N][N];

/* Set all diagonal elements to val */
void
fix_set_diag (fix_matrix A, int val)
{
  long i;
  for (i = 0; i < N; i++)
    A[i][i] = val;
}

/* Optimized `fix_set_diag'. */
void
fix_set_diag_opt (fix_matrix A, int val)
{
  long i = 0;
  int *Aptr = &A[0][0];
  while (i != (N * N + 1))
    {
      Aptr[i] = val;
      i += N + 1;
    }
}
