long
fibonacci (long n)
{
  long a, b, c;
  
  for (a = 0, b = 1; n > 1; n--)
    {
      c = a + b;
      a = b;
      b = c;
    }
  if (n == 0)
    return a;
  return b;
}

/* Fibonacci function in guarded-do transformation. */
long
fibonacci_guarded_do (long n)
{
  long a, b, c;

  a = 0;
  b = 1;
  if (n <= 1)
    goto done;

 loop:
  c = a + b;
  a = b;
  b = c;
  if (n > 1)
    goto loop;

 done:
  if (n == 0)
    return a;
  return b;
}
