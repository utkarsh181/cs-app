#+title: Practise Problem 3.51
#+author: Utkarsh Singh

| T_x    | T_y    | Instructions                                            |
|--------+--------+---------------------------------------------------------|
| long   | double | vcvtsi2sdq %rdi, %xmm0, %xmm0                           |
| double | int    | vcvttsd2si %xmm0, %edx                                  |
| double | float  | vunpcklpd %xmm0, %xmm0, %xmm0 \n vcvtpd2ps %xmm0, %xmm0 |
| long   | float  | vcvtsi2ssq %rdi, %xmm0, %xmmo                           |
| float  | long   | vcvttss2siq %xmm0, %rdx                                 |

